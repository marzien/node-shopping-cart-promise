const express = require('express')
const router = express.Router()
const ExtendedError = require('../services/error-prototypes').ExtendError

const Product = require('../models/product')

router.get('/products', (req, res, next) => {
	Product.find({})
		.exec()
		.then((products) => res.json(products))
		.catch((err) => next(new ExtendedError('Can\'t get products', 500, err)))
})

router.get('/product/:_id', (req, res, next) => {
	Product.findById({ _id: req.params._id })
		.exec()
		.then((product) => res.json(product))
		.catch((err) => next(new ExtendedError('Wrong product ID', 500, err)))
})

router.post('/product/', (req, res, next) => {
	let product = new Product(req.body)
	product.save()
		.then((product) => res.json(product))
		.catch((err) => next(new ExtendedError('Can\'t create product', 500, err)))
})

router.put('/product/:_id', (req, res, next) => {
	let id = req.params._id
	let priceChanged = req.body.price
	let quantityChanged = req.body.quantity
	let urlChanged = req.body.rul
	let typeChanged = req.body.type
	let titleChanged = req.body.title
	Product.update({ _id: id },
		{ $set: {
			title: titleChanged,
			type: typeChanged,
			urlChanged: urlChanged,
			quantity: quantityChanged,
			price: priceChanged	} })
		.exec()
		.then((product) => { res.json(product) })
		.catch((err) => next(new ExtendedError('Can\'t change product price or quantity', 500, err)))
})

router.delete('/product/:_id', (req, res, next) => {
	let id = req.params._id
	//Product.findById(id).lean().then((p) => console.log(p));
	Product.remove({ _id: id })
		.exec()
		.then((product) => res.json(product))
		.catch((err) => next(new ExtendedError('Can\'t delete product', 500, err)))
})

module.exports = router