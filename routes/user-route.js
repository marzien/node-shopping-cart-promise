const express = require('express')
const router = express.Router()
const ExtendedError = require('../services/error-prototypes').ExtendError

const User = require('../models/user')

router.get('/users', (req, res, next) => {
	User.find({})
		.exec()
		.then((products) => { res.json(products) })
		.catch((err) => { next(new ExtendedError('Can\'t get users', 500, err)) })
})

router.post('/user', (req, res, next) => {
	let user = new User(req.body)
	user.save()
		.then((product) => { res.json(product) })
		.catch((err) => { next(new ExtendedError('Can\'t create user', 500, err)) })
})

router.put('/user/:_id', (req, res, next) => {
	let id = req.params._id
	let update = {
		firstName: req.body.firstName || res.firstName,
		lastName: req.body.lastName || res.lastName,
		money: req.body.money || res.money,
	}

	User.findByIdAndUpdate(id, update)
		.exec()
		.then((user) => { res.json(user) })
		.catch((err) => { next(new ExtendedError('Can\'t update user information', 500, err)) })
})

router.delete('/user/:_id', function (req, res, next) {
	let id = req.params._id
	User.remove({ id })
		.exec()
		.then((user) => { res.json(user) })
		.catch((err) => { next(new ExtendedError('Can\'t delete user', 500, err)) })
})

module.exports = router