const express = require('express')
const router = express.Router()
const ExtendedError = require('../services/error-prototypes').ExtendError

const Order = require('../models/order')
const User = require('../models/user')
const Product = require('../models/product')

router.get('/orders', (req, res, next) => {
	Order.find({})
		.exec()
		.then((orders) => { res.json(orders) })
		.catch((err) => { next(new ExtendedError('Can\'t get oders ', 500, err)) })
})
router.post('/order', (req, res, next) => {
	let productID = req.query.product
	let orderQuant = req.query.quantity
	let userID = req.query.user
	Promise.all([
		User.findById(userID).select('money').exec(),
		Product.findById(productID).select('quantity price').exec(),
	]).then(([user, product]) => {
		if (orderQuant > product.quantity) {
			throw new ExtendedError('Not enough product quantity in shop', 400)
		} else if (user.money < product.price * orderQuant) {
			throw new ExtendedError('Not enough user\'s money for purchase', 400)
		}
		createOrder(userID, productID, orderQuant)
			.then(() => deductMoney(user, product, orderQuant))  //side effect?? () => 
			.catch((err) => { throw new ExtendedError('Unsuccesfull update user money', 400, err) })
			.then(() => updateQuantity(product, orderQuant))
			.catch((err) => { throw new ExtendedError('Unsuccesfull update quantity at warehouse', 400, err) })
			.then((order) => { res.json(order) })
	})
		.catch((err) => {
			if (err instanceof ExtendedError) {
				return next(err)
			}
			next(new ExtendedError('Query failed', 500, err))
		})
})

let deductMoney = (user, product, orderQuant) => {
	user.money = user.money - (product.price * orderQuant)
	return user.save()
}
let updateQuantity = (product, orderQuant) => {
	product.quantity = product.quantity - orderQuant
	return product.save()
}
let createOrder = (userID, productID, orderQuant) => {
	let orderData = { user: userID, product: productID, quantity: orderQuant }
	let order = new Order(orderData)
	return order.save()
}

module.exports = router