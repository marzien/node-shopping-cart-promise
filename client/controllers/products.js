var myApp = angular.module('myApp')

myApp.controller('ProductsController', ['$scope', '$http', '$location', '$routeParams',
	function ($scope, $http, $location, $routeParams) {
		$scope.makeOrder = function (prodID, q, userID) {
			$http.post('/order?product='+prodID+'&quantity='+q+'&user='+userID, $scope.product)
				.then(function () {
					alert('Order accepted!')
					$location.path('#/')
				})
				.catch(function (err) {
					$scope.err = err.data
				})
		}

		$scope.getProducts = function () {
			$http.get('/products').then(function (response) {
				$scope.products = response.data
			})
		}

		$scope.getProduct = function () {
			let id = $routeParams.id
			$http.get('/product/'+id).then(function (response) {
				$scope.product = response.data
			})
		}

		$scope.addProduct = function () {
			$http.post('/product/', $scope.product).then(function () {
				$location.path('#/products')
			})
		}

		$scope.updateProduct = function () {
			let id = $routeParams.id
			$http.put('/product/'+id, $scope.product).then(function () {
				$location.path('#/products')
			})
		}

		$scope.removeProduct = function (id) {
			$http.delete('/product/'+id).then(function () {
				$location.path('#/products')
			})
		}
	}])


	