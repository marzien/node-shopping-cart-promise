var myApp = angular.module('myApp')

myApp.controller('UserController', ['$scope', '$http', '$location', '$routeParams',
	function ($scope, $http, $location, $routeParams) {
		//TODO: user controller funcionality
		$scope.getUser = function () {
			let id = $routeParams.id
			$http.get('/user/'+id).then(function (response) {
				$scope.user = response.data
			})
		}
	}])

