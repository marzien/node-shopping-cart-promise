const User = require('../models/user')
const mongoose = require('mongoose')
const eachSeries = require('async/eachSeries')

var uristring = 'mongodb://admin:user123@ds157833.mlab.com:57833/shopping-cart'  ||  //mLab DB
				'mongodb://localhost/shopping-cart'                                 //local DB

mongoose.Promise  = global.Promise
mongoose.connect(uristring, (err, next) => {
	if (err) {
		next(new ExtendedError('Can\'t connect to database:' + uristring, 500, err))
	} else {
		console.log('Succeeded connect to: ' + uristring)
	}
})

let users =  [
	new User({
		firstName: 'testuser1f',
		lastName: 'testuser1s',
		money: 100,
	}),
	new User({
		firstName: 'testuser2f',
		lastName: 'testuser2s',
		money: 200,
	}),
	new User({
		firstName: 'testuser3f',
		lastName: 'testuser3s',
		money: 300,
	}),
	new User({
		firstName: 'testuser4f',
		lastName: 'testuser1s',
		money: 400,
	}),
	new User({
		firstName: 'testuser5f',
		lastName: 'testuser5s',
		money: 500,
	}),
]

eachSeries(users, (user, asyncdone) => {
	user.save(asyncdone)
}, (err) => {
	if (err) {
		console.log('Can\'t write to database:' + err)
	}
	mongoose.disconnect()
})